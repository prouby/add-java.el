;;; Copyright (C) 2019 by Pierre-Antoine Rouby <contact@parouby.fr>

;;; Memo java:
;;;   Multiple packages compilation
;;;     javac -d . -cp . <subpkg>/<file>.java
;;;     java <pkg>.<class>

;;; TODO: Move local variables to dir-locals.el.

(defvar java-header "")
(setq java-header "
// Generate with add-java.el
// Sources: https://framagit.org/prouby/add-java.el\n\n")

(defvar java-footer "")
(setq java-footer "\n
// Local Variables:
// mode: java
// indent-tabs-mode: nil
// eval: (local-set-key (kbd \"M-q\") 'java-indent-file)
// eval: (local-set-key (kbd \"C-c t\") 'java-wrap-region-on-try-catch)
// eval: (local-set-key (kbd \"C-c n\") 'java-new-class-in-file)
// eval: (local-set-key (kbd \"C-c f\") 'java-insert-public-fn)
// End:\n")

(defvar java-current-package "")
(setq java-current-package
      (car (last (remove "" (split-string default-directory "/")))))

(defun list-start-at (item lst)
  (cond
   ((equal lst '()) '())
   ((equal item (car lst)) lst)
   (t (list-start-at item (cdr lst)))))

(defun java-package-path-list-from (path)
  (let ((pkgl (remove ""
                      (mapcar (lambda (x) (if (string-match ".java" x) "" x))
                              (list-start-at java-current-package
                                             (split-string path "/"))))))
    (if (equal pkgl '())
        java-current-package
      (mapconcat (lambda (x) x)
                 pkgl "."))))

(defun format-header-package (path)
  (format "package %s;\n"
          (java-package-path-list-from path)))

(defun format-func-attrs (attrs)
  "'(int:x int:y) -> int x, int y."
  (mapconcat (lambda (attr)
               (let ((x (split-string attr ":")))
                 (format "%s %s" (nth 0 x) (nth 1 x))))
             attrs ", "))

(defun format-ctor-core (attrs)
  "Create default ctor function core from ATTRS."
  (mapconcat (lambda (attr)
               (let ((x (split-string attr ":")))
                 (format "this.%s = %s;" (nth 1 x) (nth 1 x))))
             attrs "\n    "))

(defun format-ctor (name extends attrs)
  "Create new ctor for NAME with ATTRS attributes."
  (format "  public %s(%s) {\n%s    %s\n  }\n"
          name
          (format-func-attrs attrs)
          (if (string-equal extends "")
              ""
            "    super();\n")
          (format-ctor-core attrs)))

(defun format-extends (extends)
  "Add class extends."
  (if (string-equal extends "")
      extends
    (format "extends %s" extends)))

(defun format-implements (implements)
  "Add class implements."
  (if (string-equal implements "")
      implements
    (format "implements %s" implements)))

(defun format-attributes (attrs)
  "Add private attributes on class."
  (if (equal attrs '())
      ""
    (let ((attr (split-string (car attrs) ":")))
      (format "  private %s %s;\n%s"
              (nth 0 attr) (nth 1 attr)
              (format-attributes (cdr attrs))))))

(defun format-getter-setter (attrs)
  "Add getters and setters."
  (if (equal attrs '())
      ""
    (let ((attr (split-string (car attrs) ":")))
      (format "  %s\n  %s\n%s"
              (format "public %s get%s() { return this.%s; }"
                      (nth 0 attr) (capitalize (nth 1 attr)) (nth 1 attr))
              (format "public void set%s(%s %s) { this.%s = %s; }"
                      (capitalize (nth 1 attr)) (nth 0 attr)
                      (nth 1 attr) (nth 1 attr) (nth 1 attr))
              (format-getter-setter (cdr attrs))))))

(defun java-public-fn (name ret-type parameters &optional core)
  "Insert new function named NAME with return type RET-TYPE and
  ARGS parameters."
  (let ((params (if (or (equal nil parameters)
                        (equal "" parameters))
                    nil
                  (split-string parameters ","))))
    (format
     "  public %s %s (%s) {\n    %s\n  }"
     ret-type name
     (if params
         (format-func-attrs params)
       "")
     (if core
         core
       "/* TODO */"))))

(defun format-toString ()
  "Return toString function template."
  (java-public-fn "toString" "String" ""))

(defun format-cmp (class1 class2 attr)
  (let* ((l (split-string attr ":"))
         (type (nth 0 l))
         (var (nth 1 l)))
    (format (if (equal "String" type)
                "%s.%s.equals (%s.%s)"
                "%s.%s == %s.%s")
            class1 var
            class2 var)))

(defun format-equals (name attrs)
  "Return toString function template."
  (let ((cvar (string-to-char (downcase name))))
    (java-public-fn
     "equals" "boolean" "Object:obj"
     (format
      "if (this == obj) return true;
    if (this == null) return false;
    if (this.getClass() != obj.getClass()) return false;

    %s %c = (%s) obj;
    if (%s)
      return true;

    return false;"
      name cvar name
      (mapconcat (lambda (x) (format-cmp "this" (char-to-string cvar) x))
                 attrs " &&\n        ")))))

(defun java-insert-new-class (name extends implements attributes &optional ext)
  "Insert new class on current buffer.

Exemple:
  M-x java-insert-new-class RET Point2D RET Shape2D RET int:x,int:y,String:name RET
"
  (interactive "sName : \nsExtends: \nsImplements: \nsAttributes: \n")
  (let ((attrs (split-string attributes ",")))
    (insert
     (format
      "public class %s %s %s {\n\n%s\n%s\n%s\n%s\n}"
      ;; Class
      name
      (format-extends extends)
      (format-implements implements)
      ;; Class core
      (format-attributes attrs)
      (format-ctor name extends attrs)
      (format-getter-setter attrs)
      (if (not ext)
          ""
        (concat
          (format-toString) "\n\n"
          (format-equals name attrs)))))))

(defun java-insert-new-interface (name functions)
  "Insert a new interface on current buffer."
  (interactive "sName: \nsFunctions (<ret:name>[,...]): \n")
  (let ((funs (split-string functions ",")))
    (insert
     (format
      "%s\ninterface %s {\n%s\n}"
      java-header
      name
      (mapconcat (lambda (fn)
                   (let ((l (split-string fn ":")))
                     (format "  public %s %s (/*TODO*/);"
                             (nth 0 l) (nth 1 l))))
                 funs "\n")))))

(defun java-new-interface-in-file (name functions)
  "Insert new interface NAME on a new file \"NAME.java\"."
  (interactive "sName: \nsFunctions (<ret:name>[,...]): \n")
  (let* ((file (format "%s.java" name))
         (buf  (generate-new-buffer file)))
    (with-current-buffer buf
      (java-insert-new-interface name functions)
      (write-file file nil))
    (find-file file)
    (java-mode)))

(defun java-new-class-in-file (path name extends implements attributes &optional ext)
  "Insert new classe NAME on a new file \"NAME.java\"."
  (interactive "DPath: \nsName : \nsExtends: \nsImplements: \nsAttributes: \n")
  (let* ((file (format "%s/%s.java" path name))
         (buf  (generate-new-buffer file)))
    (with-current-buffer buf
      (insert java-header)
      (insert (format-header-package file))
      (java-insert-new-class name extends implements attributes ext)
      (insert java-footer)
      (write-file file nil))
    (find-file file)
    (java-mode)))

(defun java-new-ext-class-in-file (path name extends implements attributes)
  "Insert new extended classe NAME on a new file \"NAME.java\"."
  (interactive "DPath: \nsName : \nsExtends: \nsImplements: \nsAttributes: \n")
  (java-new-class-in-file path name extends implements attributes t))

(defun java-insert-new-getter-setter (attributes)
  "Add getter and setter for ATTRIBUTES."
  (interactive "sAttributes: ")
  (let ((attrs (split-string attributes ",")))
    (insert
     (format-getter-setter attrs))))

(defun java-insert-main ()
  "Insert main function."
  (interactive)
  (insert (java-public-fn "main" "static void"
                          "String[]:args")))

(defun java-wrap-region-on-try-catch ()
  "Wrap region on try catch structure."
  (interactive)
  (let ((region (delete-and-extract-region (region-beginning)
                                           (region-end))))
    (insert
     (format "try {\n%s\n} catch /*TODO*/ {\n}"
             region))))

(defun java-insert-public-fn (name ret-type parameters)
  "Insert new function named NAME with return type RET-TYPE and
  ARGS parameters."
  (interactive "sName: \nsReturn type: \nsParameters: \n")
  (insert (java-public-fn name ret-type parameters)))

(defun java-indent-file ()
  "Indent current file."
  (interactive)
  (indent-region (point-min) (point-max)))


(defun java-set-current-package! (name)
  "Set current package name."
  (interactive "sName: ")
  (setq java-current-package name))
